import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.teal,
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                backgroundImage: AssetImage('images/avatar.png'),
                radius: 50.0,
              ),
              Text(
                'Andrii Kleba',
                style: TextStyle(
                    fontFamily: 'Pacifico',
                    color: Colors.white,
                    fontSize: 30.0,
                    letterSpacing: 2.5,
                    fontWeight: FontWeight.w600),
              ),
              Text('FLUTTER DEVELOPER',
                  style: TextStyle(
                    fontFamily: 'SourceSansPro',
                    color: Colors.teal[100],
                    fontSize: 19.0,
                    letterSpacing: 3.0,
                    fontWeight: FontWeight.w400,
                  )),
              SizedBox(
                height: 10.0,
                width: 230.0,
                child: Divider(
                  color: Colors.teal.shade100,
                ),
              ),
              Card(
                  margin:
                      EdgeInsets.symmetric(vertical: 15.0, horizontal: 40.0),
                  child: ListTile(
                    leading: Icon(
                      Icons.perm_phone_msg,
                      color: Colors.teal,
                    ),
                    title: Text(
                      '+38 (096) 702-44-74',
                      style: TextStyle(
                          color: Colors.teal.shade800,
                          fontFamily: 'SourceSansPro',
                          fontSize: 17.0),
                    ),
                  )),
              Card(
                  margin: EdgeInsets.fromLTRB(40.0, 2.0, 40.0, 20.0),
                  child: ListTile(
                    leading: Icon(
                      Icons.email,
                      color: Colors.teal,
                    ),
                    title: Text(
                      'console44by@gmail.com',
                      style: TextStyle(
                          color: Colors.teal.shade800,
                          fontFamily: 'SourceSansPro',
                          fontSize: 17.0),
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
